#!/usr/bin/env python
# encoding: utf-8

##Std Lib Imports:
import gc
import os
import string
import sys

##Outside imports:
from fiknight.fsm.core import *
from fiknight.fsm.export.networkx import *
import PySide2 as ps
from PySide2.QtWidgets import *
from PySide2.QtCore import QTimer, Slot, QDate, QFile, Qt
from PySide2.QtCharts import *
from PySide2.QtGui import *
from typing import Dict, List, Tuple, Union

##Within module imports:
from ce import CodeEditor

# self.hide()                                             # --- close()
#     self.window2 = Window2(memorysize, numberofholes)       # --- self
#     self.window2.show()

def mainwindowSetup(w: object) -> None:
    w.setWindowTitle("State Machine Builder")
    w.setWindowIcon(QIcon('./assets/icons/window.svg'))
    return

class MainWindow(QMainWindow):
    
    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle("Entry Frame")

        ##Bring in the style sheet:
        stylesheet_path = os.path.join(os.path.dirname(__file__), 'style.css')

        with open(stylesheet_path, 'r') as f:
            self.setStyleSheet(f.read())
        
        ##Create main window pieces:
        self.buildMenu()
        self.createDockWindows(obj_ref=self)
        self.machine_list.addItem("Default Machine")
        self.createStatusBar()

        ##Content
        stackedWidget = QStackedWidget()
        stackedWidget.addWidget(MachineEditor(obj_ref=self))
        self.setCentralWidget(stackedWidget)
        

        ##Machine editing connections:
        self.centralWidget().currentWidget().add_state.clicked.connect(self.addState)
        # self.centralWidget().currentWidget().add_state_property.clicked.connect(self.addStateProperty)
        self.centralWidget().currentWidget().add_event.clicked.connect(self.addEvent)
        


    def buildMenu(self) -> None:
        #Menu options:
        self.menu = self.menuBar()
        self.machine_menu = self.menu.addMenu("&Machines")

        #MACHINE MENU

        ##New:
        new_action = QAction("&New Machine", self)
        new_action.setStatusTip("Create a new finite state machine.")
        new_action.setIcon(QIcon('./assets/icons/material/note_add-24px.svg'))
        new_action.setShortcut("Ctrl+N")
        new_action.triggered.connect(self.newMachine)
        self.machine_menu.addAction(new_action) 
        
        ##Open:
        open_action = QAction("&Open Machine", self)
        open_action.setStatusTip("Open an existing finite state machine.")
        open_action.setIcon(QIcon('./assets/icons/material/folder_open-24px.svg'))
        open_action.setShortcut("Ctrl+O")
        open_action.triggered.connect(self.openMachine)
        self.machine_menu.addAction(open_action)

        ##Save:
        save_action = QAction("&Save Machine", self)
        save_action.setStatusTip("Save the current finite state machine.")
        save_action.setIcon(QIcon('./assets/icons/material/save-24px.svg'))
        save_action.setShortcut("Ctrl+S")
        save_action.triggered.connect(self.saveMachine)
        self.machine_menu.addAction(save_action) 

        self.machine_menu.addSeparator()

        ##Print Diagram:
        exit_action = QAction("&Print", self)
        exit_action.setStatusTip("Print the diagram for the current machine.")
        exit_action.setIcon(QIcon('./assets/icons/material/print-24px.svg'))
        exit_action.setShortcut("Ctrl+P")
        exit_action.triggered.connect(self.printDiagram)
        self.machine_menu.addAction(exit_action)

        self.machine_menu.addSeparator()

        ##Exit:
        exit_action = QAction("&Exit", self)
        exit_action.setStatusTip("Quit the application.")
        exit_action.setIcon(QIcon('./assets/icons/material/power_settings_new-24px.svg'))
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.exitApp)
        self.machine_menu.addAction(exit_action)

        #STATE MENU

        self.state_menu = self.menu.addMenu("&States")

        ##New:
        new_state_action = QAction("&New State", self)
        new_state_action.setShortcut("Ctrl+T")
        self.state_menu.addAction(new_state_action)

        #VIEW MENU

        self.view_menu = self.menu.addMenu("&View")


        #ABOUT MENU
        self.help_menu = self.menuBar().addMenu("&About")

        ##View About
        view_about_action = QAction("&About", self)
        view_about_action.triggered.connect(self.about)
        self.help_menu.addAction(view_about_action)

        

        return

    def createDockWindows(self, obj_ref: object):
        self.obj_ref = obj_ref
        dock0 = QDockWidget("Machines", self)
        dock0.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        dock0.setFeatures(dock0.NoDockWidgetFeatures)
        dock0.setFeatures(dock0.DockWidgetClosable)
        self.machine_list = QListWidget(dock0)
        self.obj_ref.machine_list = self.machine_list
        dock0.setWidget(self.machine_list)
        self.addDockWidget(Qt.LeftDockWidgetArea, dock0)
        self.view_menu.addAction(dock0.toggleViewAction())


        dock1 = QDockWidget("States", self)
        dock1.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        dock1.setFeatures(dock1.NoDockWidgetFeatures)
        dock1.setFeatures(dock1.DockWidgetClosable)
        self.state_list = QListWidget(dock1)
        self.state_list.addItems((
            "Null State",
            "State_1"))
        dock1.setWidget(self.state_list)
        self.addDockWidget(Qt.LeftDockWidgetArea, dock1)
        self.view_menu.addAction(dock1.toggleViewAction())

        dock2 = QDockWidget("Events", self)
        dock2.setAllowedAreas(Qt.LeftDockWidgetArea | Qt.RightDockWidgetArea)
        dock2.setFeatures(dock2.NoDockWidgetFeatures)
        dock2.setFeatures(dock2.DockWidgetClosable)
        self.event_list = QListWidget(dock2)
        self.event_list.addItems((
            "onEntry",
            "onExit"))
        dock2.setWidget(self.event_list)
        self.addDockWidget(Qt.LeftDockWidgetArea, dock2)
        self.view_menu.addAction(dock2.toggleViewAction())

        #Signals:
        # self.machine_list.activated.connect(self.centralWidget().setCurrentIndex)
        # self.state_list.addAction()
        # self.event_list.addAction() 

        # self.customerList.currentTextChanged.connect(print('added.'))
        # self.paragraphsList.currentTextChanged.connect(self.addParagraph)

    def createStatusBar(self):
        self.statusBar().showMessage("Ready")

    def about(self):
        QMessageBox.about(self, "About FSM Builder",
                "Project to make state machine creation a joyous point-and-click experience. "
                "Serves as a front-end for the code hosted in <a href='https://gitlab.com/Trijeet/fiknight'> this repository </a> "
                "All work is &copy; 2020 Trijeet Sethi")

    @Slot()
    def exitApp(self, checked):
        '''Wrapper to Qt's quit application.'''
        QApplication.quit()
        return

    @Slot()
    def generateDiagram(self, s):
        '''Wrapper to Qt's quit application.'''
        ##ask the user for the filepath...
        getGraph(self.machine, filepath="../gen.png")
        self.centralWidget().currentWidget().fetchNetworkDiagram(path="../gen.png")
        return

    @Slot()
    def newMachine(self, checked):
        '''Creates a new FSM project.'''
        text, ok = QInputDialog().getText(self, "Create a new FSM",
                                     "Enter a name for this machine:", QLineEdit.Normal)
        self.machine = Machine(name=text)
        self.machine_list.clear()
        self.machine_list.addItem(self.machine.name)
        self.state_list.clear()
        self.event_list.clear()
        self.centralWidget().currentWidget().buildMachineDisplay(machine = self.machine)
        return

    @Slot()
    def addEvent(self, s):
        '''Add action for state form.'''
        ##Add the code to the backend
        # destinations = [self.centralWidget().]
        # this_state = Event( name=self.centralWidget().currentWidget().state_name.text(),
        #                     destination=)
        # self.machine.addState(this_state)
        # ##Publish the result to the docked list
        # self.generateDiagram(s)
        # ##Reset the editor:
        # self.centralWidget().currentWidget().clearState(s)
        # self.updateStates(s)
        # self.centralWidget().currentWidget().buildMachineDisplay(machine = self.machine)
        return

    def statePropertyUpdate(self):
        '''Add properties for state form.'''
        # self.centralWidget().currentWidget().this_state_properties[self.centralWidget().currentWidget().state_property.text()] = self.centralWidget().currentWidget().state_property_value.text()
        # print(self.centralWidget().currentWidget().this_state_properties)
        # self.centralWidget().currentWidget().state_property.clear()
        # self.centralWidget().currentWidget().state_property_value.clear()

        if not hasattr(self, "this_state_params"):
            self.this_state_params = {}
 
        for k,v in self.centralWidget().currentWidget().this_state_properties.items():
            if k in self.this_state_params.keys():
                continue
            else:
                self.this_state_params[k] = v
        
        self.centralWidget().currentWidget().this_state_properties = {}
        return

    @Slot()
    def addState(self, s):
        '''Add action for state form.'''
        ##Add the code to the backend
        self.statePropertyUpdate()
        this_state = State(name=self.centralWidget().currentWidget().state_name.text(), machine=self.machine, **self.this_state_params)
        self.machine.addState(this_state)
        ##Publish the result to the docked list
        self.generateDiagram(s)
        ##Reset the editor:
        self.centralWidget().currentWidget().clearState(s)
        self.updateStates(s)
        self.centralWidget().currentWidget().buildMachineDisplay(machine = self.machine)
        self.this_state_params = {}
        return

    def getEventItems(self) -> List[str]:
        '''getter for state items'''
        return [self.event_list.item(i) for i in range(self.event_list.count())]

    def getStateItems(self) -> List[str]:
        '''getter for state items'''
        return [self.state_list.item(i) for i in range(self.state_list.count())]

    @Slot()
    def openMachine(self, checked):
        '''Reads in an existing FSM project.'''
        print("Read machine.")
        return

    @Slot()
    def printDiagram(self, checked):
        '''Print the diagram for an existing FSM project.'''
        print("Printing machine.")
        ##Change to print the existing displayed diagram instead:
        document = self.textEdit.document()
        printer = QPrinter()

        dlg = QPrintDialog(printer, self)
        if dlg.exec_() != QDialog.Accepted:
            return
        document.print_(printer)

        self.statusBar().showMessage("Ready", 2000)
        return      
   
    @Slot()
    def saveMachine(self, checked):
        '''Saves an existing FSM project.'''
        print("Saves a machine.")
        filename, _ = QFileDialog.getSaveFileName(self,
                "Choose a file name", '.', "HTML (*.html *.htm);;Text Files (*.txt);;")
        if not filename:
            return

        file = QFile(filename)
        if not file.open(QFile.WriteOnly | QFile.Text):
            QMessageBox.warning(self, "Dock Widgets",
                    "Cannot write file %s:\n%s." % (filename, file.errorString()))
            return

        ##Text stream a save to the file:
        out = QTextStream(file)
        QApplication.setOverrideCursor(Qt.WaitCursor)
        out << self.textEdit.toHtml()
        QApplication.restoreOverrideCursor()
        return

    @Slot()
    def updateMachines(self, s):
        '''Reads in an existing FSM project.'''
        self.machine_list.clear()
        print("Read machine.")
        return

    @Slot()
    def updateStates(self, s):
        '''Reads in an existing FSM project.'''
        self.state_list.clear()
        self.state_list.addItems([i.name for i in self.machine.state_stack])
        return

    @Slot()
    def updateEvents(self, s):
        '''Reads in an existing FSM project.'''
        self.event_list.clear()
        return

class MachineEditor(QWidget):
    '''
    Class of the four-part machine editor:
    1) State editor
    2) Event editor
    3) Machine Stats/Descriptions
    4) Machine Visualization

    '''
    def __init__(self, obj_ref: object):
        QWidget.__init__(self)
        self.obj_ref = obj_ref

        self.fullgrid = QHBoxLayout()
        self.editors = QVBoxLayout()
        self.displays = QVBoxLayout()

        ##Editors:
        self.buildStateEditor()
        self.buildEventEditor()

        ##Displays:
        self.buildNetworkDisplay()
        self.buildMachineDisplay()

        ##Compile into the four-square:
        self.editors.addLayout(self.statebox)
        self.editors.addLayout(self.eventbox)
        self.displays.addLayout(self.machinebox)
        self.displays.addLayout(self.networkbox)

        ##Main piece
        self.fullgrid.addLayout(self.editors)
        self.fullgrid.addLayout(self.displays)
        self.setLayout(self.fullgrid)

    def buildStateEditor(self):
        ##State properties:
        self.this_state_properties = {}
        ##Define containers:
        self.statebox = QVBoxLayout()
        self.statenamebox = QHBoxLayout()
        self.stateeventbox = QHBoxLayout()
        self.statepropertybox = QHBoxLayout()
        self.statesubmissionbox = QHBoxLayout()

        ##Components:
        self.state_name = QLineEdit()
        self.state_name_label = QLabel("State Name:")
        self.state_combo_data = self.obj_ref.getStateItems()
        self.state_event_selector = QComboBox()
        self.state_event_selector.clear()
        self.state_event_selector.addItems(self.state_combo_data)
        self.add_state_event = QPushButton("Add Event")
        self.state_event_label = QLabel("Choose an Event to add")
        self.state_property = QLineEdit()
        self.state_property_value = QLineEdit()
        self.add_state_property = QPushButton("Add Property")
        self.state_property_label = QLabel("Enter a property to add:")
        self.state_property_value_label = QLabel("Enter property value:")
        self.add_state = QPushButton("Add State")
        self.add_state.setProperty("formSubmitButton", True)
        self.add_state.setObjectName('AddSButton')
        self.clear_state = QPushButton("Clear State")
        self.clear_state.setProperty("formSubmitButton", True)

        ##Signals:
        self.clear_state.clicked.connect(self.clearState)
        self.add_state_property.clicked.connect(self.addStateProperty)

        ##Layout:

        ###Name line:
        self.statenamebox.addWidget(self.state_name_label)
        self.statenamebox.addWidget(self.state_name)

        ###Event line:
        self.stateeventbox.addWidget(self.state_event_label)
        self.stateeventbox.addWidget(self.state_event_selector)
        self.stateeventbox.addWidget(self.add_state_event)

        ###Property line:
        self.statepropertybox.addWidget(self.state_property_label)
        self.statepropertybox.addWidget(self.state_property)
        self.statepropertybox.addWidget(self.state_property_value_label)
        self.statepropertybox.addWidget(self.state_property_value)
        self.statepropertybox.addWidget(self.add_state_property)

        ##Submit buttons:
        self.statesubmissionbox.addWidget(self.clear_state)
        self.statesubmissionbox.addWidget(self.add_state)

        ###Build the form top-to-bottom:
        self.statebox.addLayout(self.statenamebox)
        self.statebox.addLayout(self.stateeventbox)
        self.statebox.addLayout(self.statepropertybox)
        self.statebox.addLayout(self.statesubmissionbox)
        return

    def buildEventEditor(self):
        ##Define containers:
        self.eventbox = QVBoxLayout()
        self.eventnamebox = QHBoxLayout()
        self.eventbodybox = QHBoxLayout()
        self.eventbodyentry = QVBoxLayout()
        self.eventdestinationlist = QVBoxLayout()
        self.eventdestinationbox = QHBoxLayout()
        self.eventsubmissionbox = QHBoxLayout()

        ##Components:
        self.event_name = QLineEdit()
        self.event_name_label = QLabel("Event Name")

        self.event_body_editor = QTextEdit()
        self.lint_event_body = QPushButton("Lint Body")
        self.event_body_label = QLabel("Define event function here:")

        self.event_destination = QComboBox()
        self.current_destinations = QListWidget()
        self.event_destination_label = QLabel("Set possible destination for event:")
        # self.current_destinations_label  = QLabel("Current Destinations")
        # self.current_destinations_label.setAlignment(Qt.AlignVCenter)
        self.event_probability = QDoubleSpinBox()
        self.event_probability.setRange(0.0,1.0)
        self.event_probability.setSingleStep(0.05)
        self.event_probability.setValue(1.0)
        self.add_event_destination = QPushButton("Add Destination")

        self.add_event = QPushButton("Add Event")
        self.add_event.setProperty("formSubmitButton", True)
        self.add_event.setObjectName('AddEButton')
        self.clear_event = QPushButton("Clear Event")
        self.clear_event.setProperty("formSubmitButton", True)


        ##Signals:
        self.clear_event.clicked.connect(self.clearEvent)
        self.add_event_destination.clicked.connect(self.addDestination)


        ##Layout:

        ###Name line:
        self.eventnamebox.addWidget(self.event_name_label)
        self.eventnamebox.addWidget(self.event_name)

        ###Body line:
        self.eventbodyentry.addWidget(self.event_body_label)
        self.eventbodyentry.addWidget(self.event_body_editor)
        self.eventbodybox.addLayout(self.eventbodyentry)
        self.eventbodybox.addWidget(self.lint_event_body)

        ###Property line:
        self.eventdestinationbox.addWidget(self.event_destination_label)
        self.eventdestinationbox.addWidget(self.event_destination)
        self.eventdestinationbox.addWidget(self.event_probability)
        # self.eventdestinationlist.addWidget(self.current_destinations_label)
        # self.eventdestinationlist.addWidget(self.current_destinations)
        self.eventdestinationlist.addWidget(self.add_event_destination)
        self.eventdestinationbox.addLayout(self.eventdestinationlist)

        ##Submit buttons:
        self.eventsubmissionbox.addWidget(self.clear_event)
        self.eventsubmissionbox.addWidget(self.add_event)

        ###Build the form top-to-bottom:
        self.eventbox.addLayout(self.eventnamebox)
        self.eventbox.addLayout(self.eventbodybox)
        self.eventbox.addLayout(self.eventdestinationbox)
        self.eventbox.addLayout(self.eventsubmissionbox)
        return

    def buildNetworkDisplay(self):
        self.networkbox = QVBoxLayout()

        ##Components:
        self.network_label = QLabel("Event Name")
        self.network_image = QLabel(self)
        self.fetchNetworkDiagram()

        ##Compile
        self.networkbox.addWidget(self.network_image)
        return

    def buildMachineDisplay(self, machine: object = None):

        # formLayout = QFormLayout()
        # formLayout.addRow("Name", nameLineEdit)
        # formLayout.addRow("Number of States:", emailLineEdit)
        # formLayout.addRow(self.tr("&Age:"), ageSpinBox)
        # setLayout(formLayout)
        self.machinebox = QHBoxLayout()
        # self.machinebox.setLayout(formLayout)
        self.machineproperties = QVBoxLayout()

        ##Components:
        # self.machinestates = QListWidget()
        if not machine:
            self.machine_name = QLabel("None ")
            self.machine_initial_state = QLabel("")
            self.machine_nstates = QLabel("")
        else:
            self.machine_name = QLabel(f"Machine Name: {machine.name}")
            try:
                self.machine_initial_state = QLabel(f"Initial State: {machine.initial_state.name}")
            except AttributeError as e:
                self.machine_initial_state = QLabel(f"Initial State: None")
            self.machine_nstates = QLabel(f"Total Number of States: {len(machine.state_stack)}")

        ##Compile
        self.machineproperties.addWidget(self.machine_name)
        self.machineproperties.addWidget(self.machine_initial_state)
        self.machineproperties.addWidget(self.machine_nstates)
        # self.machinebox.addLayout(self.machineproperties)
        # self.machinebox.addWidget(self.machine_name)
        # self.machinebox.addWidget(self.machine_initial_state)
        # self.machinebox.addWidget(self.machine_nstates)
        # self.machinebox.addWidget(self.machinestates)
        return


    def fetchNetworkDiagram(self, path: str = None):
        if not path:
            pxmap = QPixmap("../test.png")
            self.network_image.setPixmap(pxmap)
        else:
            pxmap = QPixmap(path)
            self.network_image.setPixmap(pxmap)
        return

    #Begin slots:
    @Slot()
    def addStateProperty(self, s):
        '''Add property for state form.'''
        if self.state_property.text() in self.this_state_properties.keys():
            QMessageBox.warning(self, "FSM State Editor", "The active state already has a property with this name.")
            # print("This property already exists!")
        self.this_state_properties[self.state_property.text()] = self.state_property_value.text()
        print(self.this_state_properties)
        self.state_property.clear()
        self.state_property_value.clear()
        return

    @Slot()
    def addDestination(self, s):
        '''Clear action for state clear button.'''
        this_prob = self.event_probability.value()
        self.event_probability.setValue(1.0)
        return

    @Slot()
    def checkDisable(self, s):
        '''Validation checker: for adding an event.'''
        if not self.event_name.text():
            self.add_event.setEnabled(False)
        else:
            self.add_event.setEnabled(True)

    @Slot()
    def clearState(self, s):
        '''Clear action for state clear button.'''
        self.state_name.clear()
        self.state_property.clear()
        self.state_property_value.clear()
        return

    @Slot()
    def clearEvent(self, s):
        '''Clear action for event clear button.'''
        self.event_name.clear()
        self.event_body_editor.clear()
        self.event_destination.clear()
        return


def main():
    app = QApplication(sys.argv)
    window = MainWindow()
    mainwindowSetup(window)
    window.resize(1200, 800)
    window.show()
    app.exec_()

if __name__ == '__main__':
    sys.exit(main()) 